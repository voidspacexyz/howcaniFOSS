
The project is basically called "How Can I FOSS" (Looking for a better name yet, Free Software word is too limiting according to me)

So we all go out and talk to people about free and open source software and stuff. So what happens once we convince people that FOSS is something that they want to pursue in their life ??
There are 2 things we usually do as a next step.
1. Redirect them to the nearest GLUG/FOSS Community
2. Explain them what all we do with FOSS

But in both these cases the real ocean possibilities of FOSS gets hidden .
The most common ways of FOSS we think of is to code, write document or evagalize. 

But is that all we can do to FOSS contributions ? There are many more ways like localization, translations, making tuts, supporting in the forums, building local communities with FOSS skills , the mere exploration of multiple FOSSs, art works and more

But if you take each of it now there are multiple projects that we could contribute to. But do we know it all ??

No, most of the times we end up FOSS digital slaves. To most of FOSS means using a Linux based distro, dominantly Ubuntu (Which according to me is not even near to FOSS). 

But is that all is means to FOSS. 

I believe there is more and there is a better way to really explore where we fit in FOSS. It is important to identify how many ways we can contribute to FOSS. 

How can I find a project that is very similar to howcanimozilla or howcanifedora but more intensive and wide spread.

This project would list all the possible ways that we can FOSS with and the ocean of FOSS's that are available for each category, like, Documentation, Programming, Evangalism etc. This project would also start exploring multiple sub-categories, like, if we take Programming for example, we could be interested in Systems Development, Mobile Development, Web Development . .  on Documentation, we could be interested in, Localization, Translations, Writing Documentation etc.

But it dosent stop there, this would tell what are the possible projects, that are available under each category/sub(-sub) category. It would also potentially be able to display, projects that require help, that needs contributors etc.
Note, we are not going to make them contribute with our platform, but we just act as mere re-directors to the actual project. 

This projects would just help people identify their real interests and also enable them to identify potential projects in their stream of interest. 
Apart from that our contributions are not constrained by a local FOSS communities knowledge or what the FOSS speaker talks about, but it really helps us have the crave for more, which always helps building better communities. 

Of course this is not all, this is just the beginning. I don't know where such an idea could grow, but I am very sure it would help a lot of us in a lot better ways.